pub mod args;

use calamine::{open_workbook_auto, Reader};
use rand::Rng;
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::fs::DirEntry;
use std::path::{Path, PathBuf};
use xlsxwriter::{FormatAlignment, GridLines, Protection, Workbook};

const VOTE_PART_SEPARATOR: &str = "##";

#[derive(Debug, Clone)]
struct Vote {
    pub creature: String,
    pub person_a: String,
    pub person_b: String,
}

impl Vote {
    fn new<S: Into<String>>(creature: S, person_a: S, person_b: S) -> Vote {
        Vote {
            creature: creature.into(),
            person_a: person_a.into(),
            person_b: person_b.into(),
        }
    }

    fn to_normalized_string(&self) -> String {
        let mut parts = vec![self.person_a.as_str(), self.person_b.as_str()];
        parts.sort_unstable();
        parts.insert(0, self.creature.as_str());
        parts.join(VOTE_PART_SEPARATOR)
    }
}

type Matches = HashMap<String, Vec<String>>;

pub fn generate_ballots(
    names_file: &Path,
    creatures_file: &Path,
    instructions_file: &Option<PathBuf>,
    path: &Path,
    random: &bool,
) -> Result<(), Box<dyn Error>> {
    let names = fs::read_to_string(names_file).expect("Could not read names from file");
    let mut names: Vec<&str> = names.lines().collect();
    names.sort_unstable();
    let creatures = fs::read_to_string(creatures_file).expect("Could not read creatures from file");
    let creatures: Vec<&str> = creatures.lines().collect();
    create_ballots_from_input(&names, &creatures, path, instructions_file, random)?;
    Ok(())
}

fn create_ballots_from_input(
    names: &[&str],
    creatures: &[&str],
    path: &Path,
    instructions_file: &Option<PathBuf>,
    random: &bool,
) -> Result<(), Box<dyn Error>> {
    if !path.exists() {
        fs::create_dir(&path)?;
    }

    for name in names {
        let filepath = path.join(format!("{}.xlsx", name));
        let filepath = filepath.to_str().ok_or("broken")?;
        let workbook = Workbook::new(filepath);

        if let Some(instructions) = instructions_file {
            let format_header = workbook
                .add_format()
                .set_bold()
                .set_font_size(32.0)
                .set_align(FormatAlignment::VerticalCenter)
                .set_align(FormatAlignment::CenterAcross);
            let format_normal = workbook
                .add_format()
                .set_font_size(18.0)
                .set_align(FormatAlignment::VerticalTop);

            let mut info_sheet = workbook.add_worksheet(Some("Instructions"))?;
            info_sheet.gridlines(GridLines::HideAllGridLines);
            info_sheet.protect("arghbalrghg", &Protection::new());
            info_sheet.merge_range(0, 0, 0, 15, "How to play", Some(&format_header))?;
            info_sheet.set_row(0, 46.0, None)?;
            let lines = fs::read_to_string(instructions)?;
            for (i, line) in lines.lines().enumerate() {
                let row = 1 + i as u32;
                info_sheet.write_string(row, 0, line, Some(&format_normal))?;
                info_sheet.set_row(row, 23.0, None)?;
            }
        }

        let format_bold = workbook.add_format().set_bold();
        let format_vote_cell = workbook
            .add_format()
            .set_font_unlocked()
            .set_align(FormatAlignment::Center);

        // header
        let mut game_sheet = workbook.add_worksheet(Some("Game"))?;
        let mut protection = Protection::new();
        protection.no_select_unlocked_cells = false;
        game_sheet.protect("arghbalr3ghg", &protection);
        game_sheet.set_selection(3, 1, 3, 1);
        game_sheet.write_string(0, 0, "Name:", Some(&format_bold))?;
        game_sheet.write_string(0, 1, name, Some(&format_bold))?;

        // creatures
        game_sheet.write_string(2, 0, "name", Some(&format_bold))?;
        for (i, creature) in creatures.iter().enumerate() {
            let col = i as u16 + 1;
            game_sheet.write_string(2, col, creature, Some(&format_bold))?;
            let width = creature.len() as f64 * (8.43 / 10.0);
            game_sheet.set_column(col, col, width.max(8.43), None)?;
        }

        game_sheet.freeze_panes(3, 1);

        // names
        let mut max_name_len = 0;
        for (i, other) in names.iter().filter(|n| *n != name).enumerate() {
            game_sheet.write_string(i as u32 + 3, 0, other, None)?;
            if other.len() > max_name_len {
                max_name_len = other.len();
            }
            for c in 1..creatures.len() + 1 {
                if *random && rand::thread_rng().gen_bool(1.0 / 2.0) {
                    game_sheet.write_string(
                        i as u32 + 3,
                        c as u16,
                        "x",
                        Some(&format_vote_cell),
                    )?;
                } else {
                    game_sheet.write_blank(i as u32 + 3, c as u16, Some(&format_vote_cell))?;
                };
            }
        }
        let max_name_len = max_name_len as f64 * (8.73 / 10.0);
        game_sheet.set_column(0, 0, max_name_len.max(8.73), None)?;

        workbook.close()?;
    }
    Ok(())
}

pub fn tally_ballots_in_path(
    path: &Path,
    max_vote_percentage: &f64,
) -> Result<HashMap<String, Matches>, Box<dyn Error>> {
    let files = fs::read_dir(path)?;
    let mut votes: Vec<Vote> = vec![];
    for file in files {
        if let Ok(mut v) = process_file(file, max_vote_percentage) {
            votes.append(&mut v);
        }
    }
    // let votes: Vec<Vote> = files
    //     .filter_map(|f| process_file(f).ok())
    //     .flatten()
    //     .collect();
    println!("Done processing ballots.");

    let matches = find_matches(votes);
    Ok(matches)
}

fn process_file(
    file: std::io::Result<DirEntry>,
    max_vote_percentage: &f64,
) -> Result<Vec<Vote>, Box<dyn Error>> {
    let mut votes: Vec<Vote> = vec![];
    let path = file?.path();
    if let Some(e) = path.extension() {
        if e != "xlsx" {
            return Ok(votes);
        }
    }
    println!("Processing ballot: {}", path.display());
    match open_workbook_auto(path) {
        Err(e) => println!("\tCould not open file: {}", e),
        Ok(mut b) => {
            match b.worksheet_range("Game") {
                Some(Ok(r)) => {
                    let mut rows = r.rows();
                    let name = rows.next().unwrap().get(1).unwrap();
                    let headers = rows.nth(1).unwrap();
                    let possible_votes = (headers.len() - 1) * rows.len();
                    let max_allowed_votes =
                        (possible_votes as f64 * *max_vote_percentage).ceil() as usize;
                    for row in rows {
                        let other = &row[0];
                        for (i, creature) in headers.iter().skip(1).enumerate() {
                            let selected = !row[i + 1].is_empty();
                            if selected {
                                let vote = Vote::new(
                                    creature.to_string(),
                                    name.to_string(),
                                    other.to_string(),
                                );
                                // println!("{:?}", vote);
                                votes.push(vote);
                            }
                        }
                    }
                    if votes.len() > max_allowed_votes {
                        println!(
                            "\t{} cast too many votes! {} cast, but max is {}",
                            name,
                            votes.len(),
                            max_allowed_votes
                        );
                        votes.clear();
                    }
                }
                _ => println!("\tCould not process file. Perhaps formatting is incorrect"),
            }
        }
    }
    Ok(votes)
}

fn find_matches(votes: Vec<Vote>) -> HashMap<String, Matches> {
    let mut matches = HashMap::new();
    for vote in votes {
        let vote = vote.to_normalized_string();
        let entry = matches.entry(vote).or_insert(0);
        *entry += 1;
    }
    matches.retain(|_, v| *v == 2);
    let mut results = HashMap::new();
    for m in matches.keys() {
        let parts: Vec<&str> = m.split(VOTE_PART_SEPARATOR).collect();
        let a = results
            .entry(parts[1].to_string())
            .or_insert_with(HashMap::new);
        let a_b = a.entry(parts[2].to_string()).or_insert_with(Vec::new);
        a_b.push(parts[0].to_string());
        let b = results
            .entry(parts[2].to_string())
            .or_insert_with(HashMap::new);
        let b_a = b.entry(parts[1].to_string()).or_insert_with(Vec::new);
        b_a.push(parts[0].to_string());
    }
    results
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn matches() {
        let votes = vec![
            // adrian
            Vote::new("ant", "adrian", "zoe"),
            Vote::new("walrus", "adrian", "zoe"),
            Vote::new("dog", "adrian", "zoe"),
            Vote::new("dog", "adrian", "bob"),
            Vote::new("cat", "adrian", "zoe"),
            Vote::new("cat", "adrian", "bob"),
            Vote::new("walrus", "adrian", "bob"),
            // bob
            Vote::new("ant", "bob", "zoe"),
            Vote::new("ant", "bob", "adrian"),
            Vote::new("walrus", "bob", "zoe"),
            Vote::new("swan", "bob", "adrian"),
            Vote::new("cat", "bob", "adrian"),
            Vote::new("dog", "bob", "adrian"),
            // sally
            Vote::new("ant", "sally", "zoe"),
            // zoe
            Vote::new("ant", "zoe", "adrian"),
            Vote::new("ant", "zoe", "bob"),
            Vote::new("ant", "zoe", "sally"),
            Vote::new("dog", "zoe", "adrian"),
            Vote::new("swan", "zoe", "adrian"),
            Vote::new("cat", "zoe", "bob"),
        ];

        let matches = find_matches(votes);
        let adrian = matches.get("adrian").unwrap();
        let bob = matches.get("bob").unwrap();
        let sally = matches.get("sally").unwrap();
        let zoe = matches.get("zoe").unwrap();

        assert_eq!(2, adrian.len());
        let creatures = adrian.get("zoe").expect("Missing matches");
        assert_eq!(2, creatures.len());
        assert!(creatures.contains(&"dog".to_string()));
        assert!(creatures.contains(&"ant".to_string()));
        let creatures = adrian.get("bob").expect("Missing matches");
        println!("adrian-bob: {:?}", creatures);
        assert_eq!(2, creatures.len());
        assert!(creatures.contains(&"dog".to_string()));
        assert!(creatures.contains(&"cat".to_string()));

        assert_eq!(2, bob.len());
        let creatures = bob.get("adrian").expect("Missing matches");
        assert_eq!(2, creatures.len());
        assert!(creatures.contains(&"dog".to_string()));
        assert!(creatures.contains(&"cat".to_string()));
        let creatures = bob.get("zoe").expect("Missing matches");
        assert_eq!(1, creatures.len());
        assert!(creatures.contains(&"ant".to_string()));

        assert_eq!(1, sally.len());
        let creatures = bob.get("zoe").expect("Missing matches");
        assert_eq!(1, creatures.len());
        assert!(creatures.contains(&"ant".to_string()));

        assert_eq!(3, zoe.len());
        let creatures = zoe.get("adrian").expect("Missing matches");
        assert_eq!(2, creatures.len());
        assert!(creatures.contains(&"dog".to_string()));
        assert!(creatures.contains(&"ant".to_string()));
        let creatures = zoe.get("bob").expect("Missing matches");
        assert_eq!(1, creatures.len());
        assert!(creatures.contains(&"ant".to_string()));
        let creatures = zoe.get("sally").expect("Missing matches");
        assert_eq!(1, creatures.len());
        assert!(creatures.contains(&"ant".to_string()));
    }

    #[test]
    fn no_matches() {
        let votes = vec![
            // adrian
            Vote::new("ant", "adrian", "bob"),
            Vote::new("walrus", "adrian", "bob"),
            Vote::new("dog", "adrian", "bob"),
            Vote::new("cat", "adrian", "bob"),
            // zoe
            Vote::new("ant", "zoe", "adrian"),
            Vote::new("dog", "zoe", "adrian"),
            Vote::new("swan", "zoe", "adrian"),
        ];

        let matches = find_matches(votes);
        assert_eq!(None, matches.get("adrian"));
        assert_eq!(None, matches.get("bob"));
        assert_eq!(None, matches.get("zoe"));
    }
}
