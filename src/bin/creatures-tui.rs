use clap::Parser;
use creatures::args::{run, Creatures};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let args = Creatures::parse();
    run(args)
}
