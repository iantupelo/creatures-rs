use creatures::args::{run, Creatures};
use klask::Settings;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    klask::run_derived::<Creatures, _>(Settings::default(), |args| {
        run(args).unwrap();
    });
    Ok(())
}
