use crate::{generate_ballots, tally_ballots_in_path};
use clap::{AppSettings, Parser, Subcommand, ValueHint};
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::fs;
use std::path::PathBuf;

#[derive(clap::ArgEnum, Copy, Clone, PartialEq)]
pub enum OutputOption {
    Json,
    Table,
}

impl Display for OutputOption {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            OutputOption::Json => write!(f, "Json"),
            OutputOption::Table => write!(f, "Table"),
        }
    }
}

#[derive(Parser)]
#[clap(about, version, author)]
pub struct Creatures {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    /// Tallies completed ballots
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Tally {
        /// Path to the completed ballot files
        #[clap(required = true, parse(from_os_str), value_hint = ValueHint::DirPath)]
        path: PathBuf,
        /// How to output results
        #[clap(short, arg_enum, default_value_t = OutputOption::Table)]
        output: OutputOption,
        /// Highest percentage of possible votes allowed cast on a single ballot
        #[clap(short, default_value_t = 0.8)]
        max_vote_percentage: f64,
    },
    /// Generates empty ballots
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Generate {
        /// Newline-delimited file containing all creature names
        #[clap(required = true, short, parse(from_os_str), value_hint = ValueHint::FilePath)]
        creatures_file: PathBuf,
        /// Newline-delimited file containing all player names
        #[clap(required = true, short, parse(from_os_str), value_hint = ValueHint::FilePath)]
        names_file: PathBuf,
        /// Newline-delimited file containing game instructions to be written to ballots
        #[clap(short, parse(from_os_str), value_hint = ValueHint::FilePath)]
        instructions_file: Option<PathBuf>,
        /// Empty or new folder in which to generate ballots
        #[clap(required = true, short, parse(from_os_str), value_hint = ValueHint::DirPath)]
        path: PathBuf,
        /// Fill generated ballots with random test data
        #[clap(long)]
        random: bool,
    },
}

pub fn run(args: Creatures) -> Result<(), Box<dyn Error>> {
    match &args.command {
        Commands::Tally {
            path,
            output,
            max_vote_percentage,
        } => {
            let results = tally_ballots_in_path(path, max_vote_percentage)?;
            match output {
                OutputOption::Json => {
                    let mut path = path.clone();
                    path.push("results.json");
                    fs::write(&path, serde_json::to_string_pretty(&results)?)?;
                    println!("Wrote results to: {}", path.to_string_lossy());
                }
                OutputOption::Table => {
                    let mut names: Vec<&String> = results.keys().collect();
                    names.sort();
                    for name in names {
                        println!("{}:", name);
                        if let Some(matches) = results.get(name) {
                            let mut others: Vec<&String> = matches.keys().collect();
                            others.sort();
                            for other in others {
                                if let Some(creatures) = matches.get(other) {
                                    println!("\t{}: {}", other, creatures.join(", "));
                                }
                            }
                        }
                    }
                }
            };
        }
        Commands::Generate {
            names_file,
            creatures_file,
            instructions_file,
            path,
            random,
        } => generate_ballots(names_file, creatures_file, instructions_file, path, random)?,
    }
    Ok(())
}
