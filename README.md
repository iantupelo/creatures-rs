# Creatures

This application can be used to generate and tally ballots for the Creatures matching game at Twin Oaks.

## Usage

### Generating empty ballots

You'll need to create a few text files as input to the generator. First, create a file that contains the list
of all players, separated by a newline. See `sample_name.txt` to see how this might look.

The next file should contain the creatures, again separated by a newline. See the example in `sample_creatures.txt`.
The creatures will appear in the same order in the ballots as they are listed in this file.

Finally, you can optionally put some instructional text in a file. See `sample_instructions.txt`. This text will
be included on the first worksheet of the ballot workbook.

To generate the ballots, run the following command:

    ./creatures generate -c ./path/to/creatures.txt -n ./path/to/names.txt -i ./path/to/instructions.txt -p ./path/to/ballots

Or, if you are not including instructions:

    ./creatures generate -c ./path/to/creatures.txt -n ./path/to/names.txt -p ./path/to/ballots

The folder that will contain the ballots should be empty. It will be created if it doesn't exist.

### Tallying ballots

To print results out to the console

    ./creatures tally ./path/to/ballots

By default, ballots containing votes for more than 80% of the possible votes are rejected. You can adjust this
percentage by using the `-m` option to provide a different percentage. For example:

    ./creatures tally ./path/to/ballots -m 0.75

## Todo

- More better output. Maybe write individual files? Templates?
- Better error handling: messages, graceful exits, checking for paths existing or not, etc
- add klask (auto-gui): https://github.com/MichalGniadek/klask
  - will not work on mac. file dialog hangs. is bug in native-dialog: https://github.com/balthild/native-dialog-rs/issues/23
  - once that is fixed, and klask updates, try again (maybe file a bug ticket)
  - also beware, klask is tied to specific versions of clap
